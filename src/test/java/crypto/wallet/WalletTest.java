package crypto.wallet;

import crypto.exceptions.InvalidFileException;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class WalletTest {

    @DisplayName("Parse file successfully")
    @Test
    void testParseSuccessfully() {
        // given
        File file = new File("src/test/resources/bobs_crypto.txt");

        // when
        Wallet wallet = Wallet.parseFromFile(file);

        // then
        assertEquals(
                Arrays.asList(
                        new WalletEntry("BTC", 10),
                        new WalletEntry("ETH", 5),
                        new WalletEntry("XRP", 2000)
                ),
                wallet.getWalletEntries()
        );
    }

    @DisplayName("When input file does not exist exception must be thrown")
    @Test
    void testFileNotFoundException() {
        // given
        File file = new File("src/test/resources/non-existing-file.txt");

        // then thrown
        assertThrows(InvalidFileException.class, () ->  Wallet.parseFromFile(file));
    }

    @DisplayName("When input file has incorrect content then exception must be thrown")
    @Test
    void testInvalidFileMustThrowException() {
        // given
        File file = new File("src/test/resources/non-existing-file.txt");

        // then thrown
        assertThrows(InvalidFileException.class, () ->  Wallet.parseFromFile(file));
    }
}
