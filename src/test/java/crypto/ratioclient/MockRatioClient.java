package crypto.ratioclient;

import java.math.BigDecimal;

public class MockRatioClient implements CryptoRatioClient {

    private static final BigDecimal BTC_EURO_RATIO = new BigDecimal(10);
    private static final BigDecimal ETH_EURO_RATIO = new BigDecimal(5);
    private static final BigDecimal XRP_EURO_RATIO = new BigDecimal(1);

    @Override
    public BigDecimal getCryptoToEuroRatio(String crypto) {
        switch (crypto) {
            case "BTC": return BTC_EURO_RATIO;
            case "ETH": return ETH_EURO_RATIO;
            case "XRP": return XRP_EURO_RATIO;
            default: throw new IllegalArgumentException("Unknown crypto");
        }
    }
}
