package crypto;

import crypto.ratioclient.CryptoRatioClient;
import crypto.ratioclient.MockRatioClient;
import crypto.report.WalletSummaryInEuro;
import crypto.wallet.Wallet;
import crypto.wallet.WalletEntry;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

import static org.junit.jupiter.api.Assertions.assertEquals;

class WalletToEuroCalculatorTest {

    private static WalletToEuroCalculator walletToEuroCalculator;

    @BeforeAll
    static void setup() {
        CryptoRatioClient cryptoRatioClient = new MockRatioClient();
        walletToEuroCalculator = new WalletToEuroCalculator(cryptoRatioClient);
    }

    @Test
    @DisplayName("Test calculation for file/wallet containing BTC")
    void calculateBTCToEuro() {
        // given
        Wallet testWallet = new Wallet();
        testWallet.addEntry(new WalletEntry("BTC", 1));

        // when
        WalletSummaryInEuro summaryInEuro = walletToEuroCalculator.convertToEuro(testWallet);

        // then
        assertEquals(new BigDecimal(10), summaryInEuro.getEuroAmountFor("BTC"));
        assertEquals(new BigDecimal(10), summaryInEuro.getTotal());
    }

    @Test
    @DisplayName("Test calculation for file/wallet containing ETH")
    void calculateETHToEuro() {
        // given
        Wallet testWallet = new Wallet();
        testWallet.addEntry(new WalletEntry("ETH", 1));

        // when
        WalletSummaryInEuro summaryInEuro = walletToEuroCalculator.convertToEuro(testWallet);

        // then
        assertEquals(new BigDecimal(5), summaryInEuro.getEuroAmountFor("ETH"));
        assertEquals(new BigDecimal(5), summaryInEuro.getTotal());
    }

    @Test
    @DisplayName("Test calculation for file/wallet containing XRP")
    void calculateXRPToEuro() {
        // given
        Wallet testWallet = new Wallet();
        testWallet.addEntry(new WalletEntry("XRP", 1));

        // when
        WalletSummaryInEuro summaryInEuro = walletToEuroCalculator.convertToEuro(testWallet);

        // then
        assertEquals(new BigDecimal(1), summaryInEuro.getEuroAmountFor("XRP"));
        assertEquals(new BigDecimal(1), summaryInEuro.getTotal());
    }

    @Test
    @DisplayName("Test calculation for file/wallet containing BTC, ETH and XRP")
    void calculateSeveralCryptosToEuro() {
        // given
        Wallet testWallet = new Wallet();
        testWallet.addEntry(new WalletEntry("BTC", 1));
        testWallet.addEntry(new WalletEntry("ETH", 1));
        testWallet.addEntry(new WalletEntry("XRP", 1));

        // when
        WalletSummaryInEuro summaryInEuro = walletToEuroCalculator.convertToEuro(testWallet);

        // then
        assertEquals(new BigDecimal(10), summaryInEuro.getEuroAmountFor("BTC"));
        assertEquals(new BigDecimal(5), summaryInEuro.getEuroAmountFor("ETH"));
        assertEquals(new BigDecimal(1), summaryInEuro.getEuroAmountFor("XRP"));
        assertEquals(new BigDecimal(16), summaryInEuro.getTotal());
    }
}