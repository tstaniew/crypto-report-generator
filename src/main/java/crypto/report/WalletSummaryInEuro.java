package crypto.report;

import java.io.PrintStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class WalletSummaryInEuro {

    private final List<CryptoValueInEuro> cryptoInEuroValues = new ArrayList<>();

    public void addEntry(CryptoValueInEuro entry) {
        cryptoInEuroValues.add(entry);
    }

    public BigDecimal getEuroAmountFor(String crypto) {
        return cryptoInEuroValues.stream()
                .filter(cryptoValueInEuro -> crypto.equals(cryptoValueInEuro.getCrypto()))
                .map(CryptoValueInEuro::getAmountInEuro)
                .findFirst()
                .orElse(BigDecimal.ZERO);
    }

    public BigDecimal getTotal() {
        return cryptoInEuroValues
                .stream()
                .map(CryptoValueInEuro::getAmountInEuro)
                .reduce(BigDecimal.ZERO, BigDecimal::add);
    }

    public void print(PrintStream output) {
        cryptoInEuroValues.forEach(output::println);
        output.println("Total: " + getTotal());
    }
}
