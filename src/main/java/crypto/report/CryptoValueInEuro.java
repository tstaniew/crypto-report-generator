package crypto.report;

import java.math.BigDecimal;

public class CryptoValueInEuro {

    private final String crypto;
    private final BigDecimal amountInEuro;

    public CryptoValueInEuro(String crypto, BigDecimal amountInEuro) {
        this.crypto = crypto;
        this.amountInEuro = amountInEuro;
    }

    public CryptoValueInEuro(String crypto, int amountInEuro) {
        this.crypto = crypto;
        this.amountInEuro = new BigDecimal(amountInEuro);
    }

    public String getCrypto() {
        return crypto;
    }

    public BigDecimal getAmountInEuro() {
        return amountInEuro;
    }

    @Override
    public String toString() {
        return "crypto=" + crypto +", amountInEuro=" + amountInEuro;
    }
}
