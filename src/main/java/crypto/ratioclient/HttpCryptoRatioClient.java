package crypto.ratioclient;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.net.HttpURLConnection;
import java.net.URL;

public class HttpCryptoRatioClient implements CryptoRatioClient {

    @Override
    public BigDecimal getCryptoToEuroRatio(String crypto) throws IOException {
        HttpURLConnection con = createHttpConnection(crypto);
        BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
        String inputLine;
        StringBuilder content = new StringBuilder();
        while ((inputLine = in.readLine()) != null) {
            content.append(inputLine);
        }
        in.close();
        String cryptoRatioAsString = content.substring(crypto.length() + 4, content.length() - 1);
        return new BigDecimal(cryptoRatioAsString);
    }

    private HttpURLConnection createHttpConnection(String crypto) throws IOException {
        URL url = new URL("https://min-api.cryptocompare.com/data/price?fsym=" + crypto + "&tsyms=EUR");
        HttpURLConnection con = (HttpURLConnection) url.openConnection();
        HttpURLConnection.setFollowRedirects( true );
        con.setConnectTimeout(20000);
        con.setRequestMethod("GET");
        con.setRequestProperty("Accept", "application/json");

        return con;
    }
}
