package crypto.ratioclient;

import java.io.IOException;
import java.math.BigDecimal;

public interface CryptoRatioClient {
    BigDecimal getCryptoToEuroRatio(String crypto) throws IOException;
}
