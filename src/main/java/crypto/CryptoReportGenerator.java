package crypto;

import crypto.ratioclient.CryptoRatioClient;
import crypto.ratioclient.HttpCryptoRatioClient;
import crypto.report.WalletSummaryInEuro;
import crypto.wallet.Wallet;

import java.io.File;
import java.io.PrintStream;

public class CryptoReportGenerator {

    private final WalletToEuroCalculator walletToEuroCalculator;

    public CryptoReportGenerator(CryptoRatioClient cryptoRatioClient) {
        this.walletToEuroCalculator = new WalletToEuroCalculator(cryptoRatioClient);
    }

    public void calculateInEuroAndPrint(File cryptoFile, PrintStream output) {
        Wallet cryptoWallet = Wallet.parseFromFile(cryptoFile);
        WalletSummaryInEuro walletSummaryInEuro = walletToEuroCalculator.convertToEuro(cryptoWallet);
        walletSummaryInEuro.print(output);
    }

    public static void main(String[] args) {
        try {
            CryptoRatioClient cryptoRatioClient = new HttpCryptoRatioClient();
            CryptoReportGenerator cryptoReportGenerator = new CryptoReportGenerator(cryptoRatioClient);

            String cryptoFileName = args.length > 0 ? args[0] : "bobs_crypto.txt";
            File cryptoFile = new File(cryptoFileName);

            cryptoReportGenerator.calculateInEuroAndPrint(cryptoFile, System.out);
        } catch (Exception e) {
            System.out.println("Unexpected error occurred during crypto file processing");
        }
    }
}
