package crypto.exceptions;

public class CryptoRatioClientException extends RuntimeException {
    public CryptoRatioClientException(Throwable cause) {
        super("Problem with crypto ratio client occured ", cause);
    }
}
