package crypto.wallet;

import java.math.BigDecimal;
import java.util.Objects;


public class WalletEntry {

    private final String crypto;
    private final BigDecimal amount;

    public WalletEntry(String crypto, BigDecimal amount) {
        this.crypto = crypto;
        this.amount = amount;
    }

    public WalletEntry(String crypto, int amount) {
        this.crypto = crypto;
        this.amount = new BigDecimal(amount);
    }

    public String getCrypto() {
        return crypto;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof WalletEntry)) return false;
        WalletEntry that = (WalletEntry) o;
        return crypto.equals(that.crypto) && amount.equals(that.amount);
    }

    @Override
    public int hashCode() {
        return Objects.hash(crypto, amount);
    }
}
