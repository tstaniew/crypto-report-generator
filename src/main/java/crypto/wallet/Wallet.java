package crypto.wallet;

import crypto.exceptions.InvalidFileException;

import java.io.File;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;

public class Wallet {

    private final List<WalletEntry> walletEntries = new ArrayList<>();

    public void addEntry(WalletEntry walletEntry) {
        walletEntries.add(walletEntry);
    }

    public List<WalletEntry> getWalletEntries() {
        return new ArrayList<>(walletEntries);
    }

    public static Wallet parseFromFile(File cryptoFile) {
        try {
            Wallet wallet = new Wallet();
            Files.lines(cryptoFile.toPath())
                    .map(Wallet::parseLineToWalletEntry)
                    .forEach(wallet::addEntry);
            return wallet;
        } catch (Exception e) {
            throw new InvalidFileException();
        }
    }

    private static WalletEntry parseLineToWalletEntry(String line) {
        String[] split = line.split("=");
        String crypto = split[0];
        BigDecimal amount = new BigDecimal(split[1]);
        return new WalletEntry(crypto, amount);
    }
}
