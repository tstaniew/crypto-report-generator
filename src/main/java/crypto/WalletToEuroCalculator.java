package crypto;

import crypto.exceptions.CryptoRatioClientException;
import crypto.ratioclient.CryptoRatioClient;
import crypto.report.CryptoValueInEuro;
import crypto.report.WalletSummaryInEuro;
import crypto.wallet.Wallet;

import java.math.BigDecimal;
import java.util.function.Function;

public class WalletToEuroCalculator {

    private final CryptoRatioClient cryptoRatioClient;

    public WalletToEuroCalculator(CryptoRatioClient cryptoRatioClient) {
        this.cryptoRatioClient = cryptoRatioClient;
    }

    public WalletSummaryInEuro convertToEuro(Wallet wallet) {
        WalletSummaryInEuro walletSummaryInEuro = new WalletSummaryInEuro();

        wallet.getWalletEntries()
                .stream()
                .map(wrap((we) -> {
                        BigDecimal cryptoToEuroRatio = cryptoRatioClient.getCryptoToEuroRatio(we.getCrypto());
                        return new CryptoValueInEuro(we.getCrypto(), we.getAmount().multiply(cryptoToEuroRatio));
                }))
                .forEach(walletSummaryInEuro::addEntry);

        return walletSummaryInEuro;
    }

    private static <T, R> Function<T, R> wrap(ThrowingFunction<T, R, Exception> throwingFunction) {
        return i -> {
            try {
                return throwingFunction.apply(i);
            } catch (Exception ex) {
                throw new CryptoRatioClientException(ex);
            }
        };
    }
}

@FunctionalInterface
interface ThrowingFunction<T, R, E extends Exception> {
    R apply(T t) throws E;
}