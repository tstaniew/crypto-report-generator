### How to build:
```shell
mvn clean install
# this will produce crypto-report-generator-1.0-SNAPSHOT.jar file in the target directory
```

### How to execute tests:
```shell
mvn test
```

### How to run:
```shell
java -jar crypto-report-generator-1.0-SNAPSHOT.jar
# By default bobs_crypto.txt will be taken
# If you want to override the file name, just pass it as a first argument of the program as presented below:
java -jar crypto-report-generator-1.0-SNAPSHOT.jar antoher-file.txt
```